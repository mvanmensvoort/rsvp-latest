class Post < ActiveRecord::Base
  belongs_to :user , :foreign_key => 'user_id'
  has_many :comments 
  attr_accessible :title, :image, :remote_image_url
  mount_uploader :image, ImageUploader
  
end
