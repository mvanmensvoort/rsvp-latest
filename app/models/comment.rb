class Comment < ActiveRecord::Base
  belongs_to :post, :foreign_key => 'post_id'
  belongs_to :user , :foreign_key => 'user_id'
  attr_accessible :body, :commenter, :user_id, :post_id
end
