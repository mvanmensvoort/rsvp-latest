class User < ActiveRecord::Base
  attr_accessible :answer, :naam, :password, :urlname, :reason
  has_many :posts   
  has_many :comments
  
  validates :naam, presence: true,
                    length: { minimum: 5 }
  validates :password, presence: true,
                    length: { minimum: 5 }
                    
                
  def self.authenticate(naam, password)
    user = User.find_by_naam(naam)
    if user && user.password == password
      user
    else
      nil
    end
  end
end
