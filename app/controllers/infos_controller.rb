class InfosController < ApplicationController
  skip_before_filter :require_login, :only=>[:index, :nieuws]
  skip_before_filter :require_admin, :only=>[:index, :nieuws]
  def index
    @infos = Info.all
  end
  
  def nieuws
    @infos = Info.all
  end
  
  def new
  @info = Info.new
  end
 
  def create
  @info = Info.new(params[:info].permit(:titel, :cont))
 
  if @info.save
    redirect_to @info, :notice => "Infomatie stuk toegevoegd "
  else
    render 'new'
  end
  end

  def show
  @info = Info.find(params[:id])
  end
  
  def edit
  @info = Info.find(params[:id])
  end
  
  def update
  @info = Info.find(params[:id])
 
  if @info.update_attributes(params[:info].permit(:titel, :cont))
    redirect_to @info 
  else
    render 'edit'
  end
end
  
  def destroy
  @info = Info.find(params[:id])
  @info.destroy
 
  redirect_to info_path
end
  
  
private
  def user_params
    params.require(:info).permit(:titel, :cont)
  end
end
