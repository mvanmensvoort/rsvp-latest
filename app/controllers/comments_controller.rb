class CommentsController < ApplicationController
  helper_method :user_finder
  skip_before_filter :require_admin, :only=>[:create]
  def create
    @post = Post.find(params[:post_id])
    
    
    @comment = @post.comments.create(params[:comment].permit(:commenter, :body).merge(:user_id => @current_user.id))
    
   
   
    redirect_to post_path(@post)
  end
  
  
   
end
