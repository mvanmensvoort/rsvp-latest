class PostsController < ApplicationController
  
  skip_before_filter :require_admin, :only=>[:new,:create, :fotoboek, :show]
  def index
    @posts = Post.all
  end
def new
  @post = Post.new
end
 
def create
  
   @current_user ||= User.find(session[:user_id]) if session[:user_id]
    @post = @current_user.posts.create(params[:post].permit(:title, :image, :remote_image_url))
    redirect_to post_path(@post)
 
end

def edit
  @post = Post.find(params[:id])
end
  def update
  @post = Post.find(params[:id])
 
  if @post.update_attributes(params[:post].permit(:title, :image, :remote_image_url))
    redirect_to @post
  else
    render 'edit'
  end
end
 
  def show
   @post = Post.find(params[:id])
  end
  def fotoboek
    @posts = Post.all
  end
  def destroy
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
    @post = Post.find(params[:id])
    
    @post.destroy
    redirect_to posts_path
  end
private
  def post_params
    params.require(:post).permit(:title)
  end
end
