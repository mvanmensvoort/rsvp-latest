class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user
  helper_method :admin
 
before_filter :require_login
before_filter :require_admin  

private


  
def current_user
  @current_user ||= User.find(session[:user_id]) if session[:user_id]
  
end

def admin
  @admin = User.find_by_naam("Scarlett")
end

def require_admin
  if admin == current_user
  
  else
    redirect_to root_path
  end
end

def require_login
    unless current_user
      redirect_to log_in_path
    end
  end
end


