class UsersController < ApplicationController
  skip_before_filter :require_login, :only=>[:rsvpshow, :update, :new, :create, :newrsvp, :show]
  skip_before_filter :require_admin, :only=>[:rsvpshow, :update, :new, :create, :show, :edit, :newrsvp]
  def index
    @users = User.all
  end
  
  def new
  @user = User.new
  end
  
  def newrsvp
    @user = User.new
  end
 
  def create
  @user = User.new(params[:user].permit(:naam, :password, :answer, :urlname, :reason))
 
  if @user.save
    redirect_to @user, :notice => "User added"
  else
    render 'new'
  end
  end

  def show
 @user = User.find(params[:id])
  end
  
  def rsvpshow
    @user = User.find_by_urlname params[:urlname]

  end
  
  def edit
  @user = User.find(params[:id])
  end
  
  def update
  @user = User.find(params[:id])
 
  if @user.update_attributes(params[:user].permit(:naam, :answer, :urlname, :password, :reason))
    if @user.id == admin.id
    redirect_to @user
    else 
    redirect_to rsvp_path(@user.urlname), :notice => "Dank je wel voor het reageren. Groetjes, Scarlett & Koen"
    end
  else
    render 'edit'
  end
end
  
  def destroy
  @user = User.find(params[:id])
  @user.destroy
 
  redirect_to users_path
end
  
  
private
  def user_params
    params.require(:user).permit(:naam, :answer, :password, :urlname, :reason)
  end
end
