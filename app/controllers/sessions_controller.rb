class SessionsController < ApplicationController
  
  skip_before_filter :require_login 
  skip_before_filter :require_admin
  def new
end

def create
  user = User.authenticate(params[:naam], params[:password])
  if user
    session[:user_id] = user.id
    redirect_to root_url, :notice => "Logged in!"
  else
    flash.now.alert = "Invalid email or password"
    render "new"
  end
end

def destroy
  session[:user_id] = nil
  redirect_to root_url, :notice => "Logged out!"
end
end
