class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.string :titel
      t.text :cont

      t.timestamps
    end
  end
end
